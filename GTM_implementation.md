# Google Tag Manager implementation

  [https://www.youtube.com/watch?v=In4TNHLTz_Y](https://www.youtube.com/watch?v=In4TNHLTz_Y)

## React/Next implementation

1. instalace react-gtm-consent-module případně vložení kódu uvedeného zde: [https://developers.google.com/tag-platform/tag-manager/web](https://developers.google.com/tag-platform/tag-manager/web)

    > import TagManager, { TagManagerArgs } from "react-gtm-consent-module";
    >
    > const tagManagerArgs: TagManagerArgs = { gtmId: window.ENV.GTM_ID }; // při použití configu a window.ENV
    >
    > TagManager.initialize(tagManagerArgs);

2. instalace nebo použití stávajícího cookie banneru a jeho nastavení (lze vložit i přímo do GTM)

    > cookie_name: **_"pcdc-cookieconsent"_**, // default: 'cc_cookie'
    >
    > cz: {
    > consent_modal: {
    > title: "Používáme cookies",
    > description:
    > "Díky využití souborů cookies zjišťujeme, co je pro uživatele zajímavé a analyzujeme naši propagaci na sociálních sítích. Analýza návštěvnosti nám pomáhá neustále zlepšovat náš web a umožňuje nám lépe připravovat i samotnou konferenci. Svůj souhlas můžete kdykoliv odvolat.Přečtěte si naše zásady o zpracovnání osobních údajů.",
    > primary_btn: {
    > text: "Povolit vše",
    > role: "accept_all", // 'accept_selected' or 'accept\*all'
    > },
    > secondary_btn: {
    > text: "Nastavení cookies",
    > role: "c-settings", // 'settings' or 'accept_necessary'
    > },
    > },
    > settings_modal: {
    > title: "Přizpůsobit nastavení cookies",
    > save_settings_btn: "Uložit nastavení",
    > accept_all_btn: "Povolit vše",
    > reject_all_btn: "Povolit nezbytné",
    > close_btn_label: "Zavřít",
    > blocks: [
    > {
    > title: "Nezbytně nutné cookies",
    > description:
    > "Tyto cookies pomáhají, aby webová stránka byla použitelná a fungovala správně. Ve výchozím nastavení jsou povoleny a nelze je zakázat.",
    > toggle: {
    > value: **_"functional_storage"_**,
    > enabled: true,
    > readonly: true, // cookie categories with readonly=true are all treated as "necessary cookies"
    > },
    > },
    > {
    > title: "Statistické",
    > description:
    > "Statistické cookies pomáhají majitelům webových stránek, aby porozuměli, jak návštěvníci používají webové stránky. Tyto informace mohou být také použity k měření účinnosti našich marketingových kampaní nebo k vytvoření personalizovaného prostředí na webu.",
    > toggle: {
    > value: **_"analytics_storage"_**, // there are no default categories => you specify them
    > enabled: false,
    > readonly: false,
    > },
    > },
    > {
    > title: "Soubory cookie třetích stran",
    > description:
    > "V některých zvláštních případech používáme také soubory cookie poskytované důvěryhodnými třetími stranami. V následující části jsou podrobně popsány soubory cookie třetích stran, se kterými se můžete na těchto stránkách setkat. Tyto stránky používají službu Google Analytics, která je jedním z nejrozšířenějších a nejdůvěryhodnějších analytických řešení na webu, které nám pomáhá porozumět tomu, jak stránky používáte, a způsobům, jakými můžeme zlepšit vaše zkušenosti. Tyto soubory cookie mohou sledovat například dobu, kterou na webu strávíte, a navštívené stránky, abychom mohli pokračovat ve vytváření zajímavého obsahu. Další informace o souborech cookie služby Google Analytics naleznete na oficiální stránce Google Analytics. Na těchto stránkách také používáme tlačítka a/nebo pluginy sociálních médií, které vám umožňují různými způsoby se propojit s vaší sociální sítí. Pro jejich fungování sociální sítě Facebook a Instagram nastaví prostřednictvím našich stránek soubory cookie, které mohou být použity k vylepšení vašeho profilu na jejich stránkách nebo přispět k údajům, které mají k dispozici pro různé účely uvedené v jejich zásadách ochrany osobních údajů.",
    > toggle: {
    > value: **_"ad_storage"_**, // there are no default categories => you specify them
    > enabled: false,
    readonly: false,
    },
    },
    ],
    },

3. obsah cookie

liší se podle druhu i verze consent banneru

příklad pcdc-cookieconsent("vanilla-cookieconsent": "^2.8.0"): {"level":["functional_storage","analytics_storage"],"revision":0,"data":null,"rfc_cookie":false}

respektive "{\"level\":[\"functional_storage\",\"analytics_storage\"],\"revision" +
"\":0,\"data\":null,\"rfc_cookie\":false}"

příklad golemiobi-cookieconsent("vanilla-cookieconsent": "^2.8.5"): {"categories":["functional_storage","analytics_storage"],"revision":0,"data":null,"rfc_cookie":false,"consent_date":"2022-09-08T19:01:06.183Z","consent_uuid":"cd61efc8-a52b-42d5-b1c2-3db917c67952","last_consent_update":"2022-09-08T20:53:05.004Z"}

## GTM configurace

1. načtení cookie

    GTM>Variables>new 1st Party Cookie

2. zapnutí Consent Overview

    GTM>Admin>Container Settings>Additional Settings>Enable consent overview

    automatické sledování hodnot cookie

3. vložení variable GTM Consent State z galerie

4. vložení a nastavení tagu Consent Mode z galerie nebo vlastního consent mode podle GTM dokumentace

5. přeložení obsahu cookie do consent state

    GTM>Variables>custom javascript

    kod pro cjs - analytics consent,

    > function() {
    > var cookie = {{golemiobi-cookieconsent}}
    > if(!cookie) {return 'denied'}
    > var str = cookie.replace(/\\/g, "");
    > var data = JSON.parse(str);
    > if(data.categories.indexOf('**_analytics_storage_**')  > -1) {
    > return 'granted'
    > } else {
    > return 'denied'
    > }
    >}

6. přidání výstupu z cjs variables do Consent Modu

7. přidání triggeru na buttony

    GTM>Triggers>Choose trigger type>Click - All elements> some clicks>Click Classes

    naslouchání přes Click Classes např.: "c-bn", zjistit se dá přes dev tools nebo rovnou v GTM

8. race conditions problem

    GTM>Tag>Custom HTML>cHTML - dataLayer push Consent

    > \<script>
    > (function() {
    > window.setTimeout(function() {window.dataLayer.push({'event': 'consentChoise'});}, 500);
    > window.setTimeout(function() {window.dataLayer.push({'event': 'consentUpdated'});}, 800);
    > })();
    > \</script>

    triggrovat přes Click - All elements

9. vytvoření triggerů consentChoise a consentUpdated

    GTM>Trigger>Custom Event

    přidání triggeru consentChoise do Consent Mode

    přidání triggeru consentUpdated kjednotlivým tagům GA, FB Pixel apod.

10. nastavení consent checks podle druhu tagu, GA umí automaticky, FB, Smartlook se nastaví přes Require additional consent for tag to fire
